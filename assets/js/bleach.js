$(document).ready(function () {
  $('#togglenav').click(function () {
    if ($('.left-panel').hasClass('collapse')) {
      $('.left-panel').removeClass('collapse')
    } else {
      $('.left-panel').addClass('collapse')
    }
  });
  
  $(".left-panel .menu-bar .menu-group:has(.menu-list > a.active)").addClass("active");
  
});