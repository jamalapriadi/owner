var ownerApp=angular.module("ownerApp",['ngRoute']);

ownerApp.config(function($routeProvider){
	$routeProvider
		.when('/',{
			templateUrl:'page/home.html',
			controller:'mainController'
		})

		.when('/profile-po',{
			templateUrl:'page/profile-po.html',
			controller:'profileController'
		})

		.when('/informasi-trayek',{
			templateUrl:'page/informasi-trayek.html',
			controller:'informasitrayekController'
		})

		.when('/data-pelayanan',{
			templateUrl:'page/data-pelayanan.html',
			controller:'pelayananController'
		})

		.when('/penjualan',{
			templateUrl:'page/penjualan.html',
			controller:'penjualanController'
		})

		.when('/reservasi',{
			templateUrl:'page/reservasi.html',
			controller:'reservasiController'
		})

		.when('/penerimaan',{
			templateUrl:'page/penerimaan.html',
			controller:'penerimaanController'
		})

		.when('/penumpang-terdaftar',{
			templateUrl:'page/penumpang-terdaftar.html',
			controller:'penumpangController'
		})

		.when('/penumpang-per-trayek',{
			templateUrl:'page/penumpang-per-trayek.html',
			controller:'pntController'
		})

		.when('/rekapitulasi',{
			templateUrl:'page/rekapitulasi.html',
			controller:'rekapitulasiController'
		})

		.when('/kas',{
			templateUrl:'page/kas.html',
			controller:'kasController'
		})

		.when('/armada',{
			templateUrl:'page/armada.html',
			controller:'armadaController'
		})

		.when('/trayek',{
			templateUrl:'page/trayek.html',
			controller:'trayekController'
		})

		.when('/agen',{
			templateUrl:'page/agen.html',
			controller:'agenController'
		})

		.when('/tarif',{
			templateUrl:'page/tarif.html',
			controller:'tarifController'
		})

		.when('/tambah-info-trayek',{
			templateUrl:'page/tambah-info-trayek.html',
			controller:'tambahinfotrayek'
		})

		.when('/detail-info-trayek',{
			templateUrl:'page/detail-info-trayek.html',
			controller:'detailinfotrayek'
		})

		.when('/tambah-detail-info-trayek',{
			templateUrl:'page/tambah-detail-info-trayek.html',
			controller:'tambahdetailinfotrayek'
		})

		.when('/tambah-agen',{
			templateUrl:'page/tambah-agen.html',
			controller:'tambahagen'
		})

		.when('/rencana-jadwal',{
			templateUrl:'page/rencana-jadwal.html',
			controller:'rencanajadwal'
		})

		.when('/tambah-rencana',{
			templateUrl:'page/tambah-rencana.html',
			controller:'tambahrencana'
		})
});

ownerApp.controller('mainController',function($scope){
	$scope.pageClass = 'merah';
});

ownerApp.controller('profileController',function($scope){
	$scope.title="Profile PO";
});

ownerApp.controller('informasitrayekController',function($scope){
	$scope.trayeks=[
		{
			seri:'L',
			lintasan:'Semin-Wonosari-Tangerang',
			subtrayek:'Tol Cikampek-Klari-Cikarang-Cibitung-Bekasi-Jatiwaringin/Kalimampang-Pancoran-Mampang-Slipi-Grogol-Cengkareng-Kalideres-Poris-Bitung-PS.Kemis-Cikupa-PP'
		},
		{
			seri:'N',
			lintasan:'Semin-Wonosari-Pulogadung',
			subtrayek:'Tol Cikampek-Klari-Cikarang-Cibitung-Bekasi-Cempaka Putih-Pulogadung-PP'
		}
	];
});

ownerApp.controller('pelayananController',function($scope){
	$scope.pageClass = 'kuning';
});

ownerApp.controller('penjualanController',function($scope){
	$scope.pageClass = 'biru';
});

ownerApp.controller('reservasiController',function($scope){
	$scope.pageClass = 'hijau';
});

ownerApp.controller('penerimaanController',function($scope){
	$scope.pageClass = 'merah';
});

ownerApp.controller('penumpangController',function($scope,$http){
	$scope.pageClass = 'kuning';
	$http.get("http://panturaweb.dev/cbt/public/api/siswa").success(function(result){
		$scope.data=result;
	});
});

ownerApp.controller('pntController',function($scope){
	$scope.pageClass = 'biru';
});

ownerApp.controller('rekapitulasiController',function($scope){
	$scope.pageClass = 'hijau';
});

ownerApp.controller('kasController',function($scope){
	$scope.pageClass = 'kuning';
});

ownerApp.controller('armadaController',function($scope){
	$scope.pageClass = 'kuning';
});

ownerApp.controller('trayekController',function($scope){
	$scope.pageClass = 'biru';
});

ownerApp.controller('agenController',function($scope){
	$scope.pageClass = 'hijau';

	$scope.agens=[
		{nama:'Agen PS Minggu / Budi Raharja',alamat:'Jl. Raya PS Minggu ( Depan Stasiun )',telp:'021-6763388'},
		{nama:'Agen Jatinegara / Liem Haryanto',alamat:'Jl. Bungur 17 ( Samping Indomaret )',telp:'021-8362328'}
	];
});

ownerApp.controller('tarifController',function($scope){
	$scope.pageClass = 'merah';
});

ownerApp.controller('tambahinfotrayek',function($scope){
	$scope.lintasans=[
		{kota:'Wonosari',subkota:'Semin'},
		{kota:'Wonosari',subkota:'Wonosari'},
		{kota:'Bandung',subkota:'Tol Bandung'},
		{kota:'Bandung',subkota:'Klari'},
		{kota:'Cikarang',subkota:'Cikarang'},
		{kota:'Cibitung',subkota:'Cibitung'},
		{kota:'Bekasi',subkota:'Bekasi'},
		{kota:'Jakarta',subkota:'Pancoran'},
		{kota:'Jakarta',subkota:'Pancoran'},
		{kota:'Jakarta',subkota:'Mampang'},
		{kota:'Jakarta',subkota:'Senayan'},
		{kota:'Tangerang',subkota:'Cipulir'},
		{kota:'Tangerang',subkota:'Cileduk'}

	];
});

ownerApp.controller('detailinfotrayek',function($scope){
	$scope.details=[
		{
			seri:'L',
			subseri:'AC VIP 44 Seat',
			kode:'MLL-001',
			trayek:'Semin-Wonosari-Tangerang',
			fasilitasbus:'Fasilitas Bus : AC,RS,FR, VID,AUD,TLT,SA,WIFI',
			fasilitasservice:'Fasilitas Service : AM,SNK,VM,BG'
		},
		{
			seri:'L',
			subseri:'AC VIP 44 Seat',
			kode:'MLL-002',
			trayek:'Semin-Wonosari-Tangerang',
			fasilitasbus:'Fasilitas Bus : AC,RS,FR, VID,AUD,TLT,SA,WIFI',
			fasilitasservice:'Fasilitas Service : AM,SNK,VM,BG'
		}
	];
});

ownerApp.controller('tambahdetailinfotrayek',function($scope){
	
});


ownerApp.controller('tambahagen',function($scope){

})

ownerApp.controller('rencanajadwal',function($scope){
	$scope.rencana=[
		{
			tgl:'18-09-2015',
			kode:'MLL-001',
			lokasi:'Agen PS Minggu',
			waktu:'19.30',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		},
		{
			tgl:'18-09-2015',
			kode:'MLL-001',
			lokasi:'Agen Jatinegara',
			waktu:'19.55',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		},
		{
			tgl:'19-09-2015',
			kode:'MLL-002',
			lokasi:'Agen PS Minggu',
			waktu:'20.30',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		},
		{
			tgl:'19-09-2015',
			kode:'MLL-002',
			lokasi:'Agen Jatinegara',
			waktu:'20.30',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		}
	];
})

ownerApp.controller('tambahrencana',function($scope){
	$scope.rencana=[
		{
			tgl:'18-09-2015',
			kode:'MLL-001',
			lokasi:'Agen PS Minggu',
			waktu:'19.30',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		},
		{
			tgl:'18-09-2015',
			kode:'MLL-001',
			lokasi:'Agen Jatinegara',
			waktu:'19.55',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		},
		{
			tgl:'19-09-2015',
			kode:'MLL-002',
			lokasi:'Agen PS Minggu',
			waktu:'20.30',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		},
		{
			tgl:'19-09-2015',
			kode:'MLL-002',
			lokasi:'Agen Jatinegara',
			waktu:'20.30',
			harga:'Rp. 108.000,-',
			komisi:'Rp. 12.000,-',
			total:'Rp. 120.000,-'
		}
	];	
})



/*
myApp.controller('mainController',function($scope,$http,$location){
	$scope.message="Setiap orang datang dan melihatnya";
	$http.get("http://sinau.dev/lv4/public/api/event").success(function(result){
		$scope.data=result;
	});
});
*/